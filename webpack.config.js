const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	devtool: 'source-map',
	entry: './app/main.ts',
	externals: {
		jquery: 'jQuery',
		leaflet: 'L'
	},
	plugins: [
		new CleanWebpackPlugin({
			cleanAfterEveryBuildPatterns: ['public']
		}),
		new CopyPlugin([
			{ from: './app/css/*', to: 'css/', flatten: true },
			{ from: './app/img/*', to: 'img/', flatten: true }
		]),
		new HtmlWebpackPlugin({
			template: './app/index.html',
			title : 'Ubiquitous Wikidata'
		}),
	],
	output: {
		path: __dirname + '/public',
		filename: '[name].js'
	},
	resolve: {
		extensions: ['.css', '.js', '.svg', '.ts', '.tsx']
	},
	module: {
		rules: [
			{ test: /\.css$/i, use: ['style-loader', 'css-loader'] },
			{ test: /\.svg$/i, use: [
				{
				  loader: 'file-loader',
				},
			  ]
			},
			{ test: /\.tsx?$/i, use: 'ts-loader' },
			{ test: /\.txt$/i, use: 'raw-loader' }
		]
	}
}